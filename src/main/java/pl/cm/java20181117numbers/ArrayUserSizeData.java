package pl.cm.java20181117numbers;

import java.util.Scanner;

public class ArrayUserSizeData {
    public static void main(String[] args) {

        // Podanie przez urzytkownika wiekości tablicy
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wielkość tablicy ");
        int x = scanner.nextInt();

        int[] arr = new int[x];


        // Wprowadzanie danych do tablicy
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" Podaj liczbę dla komórki nr " + i + ": ");
            arr[i] = scanner.nextInt();
        }

        // Wypisanie wartości max i min
        int max = arr[0];
        int min = arr[0];
        for (int a : arr) {
            if (a > max) {
                max = a;
            }
            if (a < min) {
                min = a;
            }
        }

        System.out.println("Największa liczba w tabeli to: " + max);
        System.out.println("Najmniejsza liczba w tabeli to: " + min);

    }
}


//
//        for(int a : arr) {
//            System.out.println("Podaj wartość dla kolejnej komórki: ");
//            int b = scanner.nextInt();
//        }
//
//        int max = 0;
//        for (int i = 0; i > max; i++) {
//            System.out.println(max);
//        }
//
//
//    }
//}
