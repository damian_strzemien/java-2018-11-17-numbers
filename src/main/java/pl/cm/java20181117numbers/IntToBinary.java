package pl.cm.java20181117numbers;

import java.util.Scanner;

public class IntToBinary {
    public static void main(String[] args) {

        // Użytkownik wprowadza liczbę
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę do przekonwertowania. Max liczba nie większa niż 15: ");
        byte liczba = scanner.nextByte();

        // Ostrzeżenie o zbyt dużej liczbie
        if (liczba > 15) {
            System.out.println("Przekroczyłeś maxymalną wartość");
            return;
        }
        // Przypisanie liczby do komórki w tablicy
        int[] arr = new int[1];
        arr[0] = liczba;

        // Wypisanie podanej liczby binarnie
        System.out.println("Liczba " + liczba + " zapisana binarnie to: " + Integer.toBinaryString(liczba));


    }
}
