package pl.cm.java20181117numbers;

import java.util.Scanner;

public class IntTo42 {
    /**
     * @author Damian Strzemien
     * @version v 1.0
     * @param args Ta metoda nie niczego nie zwraca
     *
     *             Nadal nie wiem co można, co należy wpisać do JavaDoc i co wnosi wartość dodaną.
     *             Może to poniżej:
     *             To są zadania domowe. IntTo42, ArrayUserSizeData oraz IntToBinary. Nie są ze sobą powiązane.
     */
    public static void main(String[] args) {

        // Umożliwienie użytkownikowi wprowadzenia danych
        Scanner scanner = new Scanner(System.in);
        int x;

        do {
            System.out.println("Wpisz liczbę całkowitą, która zamknie ten program ");
            x = scanner.nextInt();
            System.out.println("Wpisałeś " + x);

        }
        while (x != 42);

        if (x == 42) {
            System.out.println(" Brawo, to ta liczba zamyka ten program. Do zobaczenia");
        }

    }
}
